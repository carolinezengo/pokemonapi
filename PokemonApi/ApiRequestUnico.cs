﻿using Newtonsoft.Json;
using PokemonApi.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi
{

    public class ApiRequestUnico : PokemonVista
    {
        public string url;

        
        public ApiRequestUnico(int id)
        {

            url = $"https://pokeapi.co/api/v2/pokemon/{id}";


        }
     
          

    
        public DadosPokemon Pokemon()
        {

            var consulta = (HttpWebRequest)WebRequest.Create(url);
            
            consulta.Method = "GET";
            consulta.ContentType = "application/json";
            consulta.Accept = "application/json";
            try 
            {
                using (WebResponse response = consulta.GetResponse())
                {
                    using(Stream stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }
                        using (StreamReader leitor = new StreamReader(stream))
                        {
                            string responseTexto = leitor.ReadToEnd();
                          
                         DadosPokemon  pokeLista =  JsonConvert.DeserializeObject<DadosPokemon>(responseTexto);

                            
                            return pokeLista;
                        }




                    }
                }
            }
            catch (Exception) {

                throw new Exception ("Esse pokemon não existe.");
            }

            
        }
    }
}
