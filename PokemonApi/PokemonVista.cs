﻿using PokemonApi.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokemonApi
{
    public partial class PokemonVista : UserControl
    {

        
        public int id { get; set; }
        public string nomePokemon { get; set; }
        public Image image { get; set; }


        public PokemonVista()
        {
           
           
        }

       
        public PokemonVista(string nome, Image imagem,int id)
        {
          InitializeComponent();
          
            this.nomePokemon = nome;
            this.imagem.Image = imagem;
            this.id = id;

        }
        public PokemonVista(string nomePokemon, int id)
        {
            this.nomePokemon = nomePokemon;
            this.id = id;
        }

        private void PokemonVista_Load(object sender, EventArgs e)
        {
            ID.Text = id.ToString();
            nome.Text = nomePokemon;

        }

        private void imagem_Click(object sender, EventArgs e)
        {
        
            Form2 form2 = new Form2(id);
            form2.ShowDialog();
            
                
        }
         
       
        

    }
}

