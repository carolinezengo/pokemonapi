﻿
namespace PokemonApi
{
    partial class PokemonVistaUnico
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Imagem = new System.Windows.Forms.PictureBox();
            this.Nome = new System.Windows.Forms.Label();
            this.Habilidade = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tipo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Imagem)).BeginInit();
            this.SuspendLayout();
            // 
            // Imagem
            // 
            this.Imagem.Location = new System.Drawing.Point(22, 12);
            this.Imagem.Name = "Imagem";
            this.Imagem.Size = new System.Drawing.Size(194, 184);
            this.Imagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Imagem.TabIndex = 0;
            this.Imagem.TabStop = false;
            this.Imagem.Click += new System.EventHandler(this.Imagem_Click);
            // 
            // Nome
            // 
            this.Nome.AutoSize = true;
            this.Nome.Location = new System.Drawing.Point(75, 209);
            this.Nome.Name = "Nome";
            this.Nome.Size = new System.Drawing.Size(35, 13);
            this.Nome.TabIndex = 1;
            this.Nome.Text = "label1";
            // 
            // Habilidade
            // 
            this.Habilidade.AutoSize = true;
            this.Habilidade.Location = new System.Drawing.Point(83, 246);
            this.Habilidade.Name = "Habilidade";
            this.Habilidade.Size = new System.Drawing.Size(35, 13);
            this.Habilidade.TabIndex = 2;
            this.Habilidade.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nome: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Habilidade:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(137, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Tipo:";
            // 
            // tipo
            // 
            this.tipo.AutoSize = true;
            this.tipo.Location = new System.Drawing.Point(191, 209);
            this.tipo.Name = "tipo";
            this.tipo.Size = new System.Drawing.Size(35, 13);
            this.tipo.TabIndex = 6;
            this.tipo.Text = "label6";
            // 
            // PokemonVistaUnico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tipo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Habilidade);
            this.Controls.Add(this.Nome);
            this.Controls.Add(this.Imagem);
            this.Name = "PokemonVistaUnico";
            this.Size = new System.Drawing.Size(252, 303);
            this.Load += new System.EventHandler(this.PokemonVista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Imagem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Imagem;
        private System.Windows.Forms.Label Nome;
        private System.Windows.Forms.Label Habilidade;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label tipo;
    }
}
