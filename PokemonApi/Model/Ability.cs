﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi.Model
{
  public  class Ability

    {
        [JsonProperty("Ability")]
        public Species Habilidade1 { get; set; }

        [JsonProperty("is_hidden")]
        public bool Condicao { get; set; }

        [JsonProperty("slot")]
        public int idhabilidade { get; set; }


    }
}
