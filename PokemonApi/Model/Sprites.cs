﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi.Model
{
    public class Sprites
    {
      
        [JsonProperty("front_default")]

        public Uri Link { get; set; }
        public object FrontFemale { get; set; }


        public Image GetImage()
        {
           
          
            string dir = Link.ToString();
            
          

            var request = WebRequest.Create(dir);
            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    return Bitmap.FromStream(stream);
                }

            }

        }
    }
 }
