﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi.Model
{
   public class Species
    {
        [JsonProperty("Name")]
        public string Nome { get; set; }

        [JsonProperty("Url")]
        public Uri Link{ get; set; }
        
    }
}
