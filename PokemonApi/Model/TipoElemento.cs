﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi.Model
{
   public class TipoElemento
    {

        [JsonProperty("Slot")]
        public long idtipo { get; set; }

        [JsonProperty("Type")]
        public Species Tipo { get; set; }
       
    }
}
