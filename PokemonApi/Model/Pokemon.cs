﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi.Model
{
   public class Pokemon
    {
      
        public int ID{ get; set; }

        [JsonProperty("name")]
        public string NomePokemon { get; set; }

        [JsonProperty("Url")]
        public Uri Link{ get; set; }

        [JsonProperty("Abilities")]
        public Ability[] Habilidade { get; set; }

        [JsonProperty("Types")]
        public TipoElemento[] Tipo { get; set; }

        public Image GetImage()
        {
            string dir = Link.ToString();
            dir = dir.Substring(0, dir.Length - 1);
            dir = dir.Substring(dir.LastIndexOf("/"));

            dir = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + dir + ".png";

            var request = WebRequest.Create(dir);
            using (var response = request.GetResponse())
            {
                using(var stream = response.GetResponseStream())
                {
                    return Bitmap.FromStream(stream);
                }

            }
                
        }

    }
}
