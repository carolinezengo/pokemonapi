﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi.Model
{
    public class ListaPokemon
    {
        [JsonProperty("count")]
        public long Quantidade { get; set; }

        [JsonProperty("next")]
        public string Proximo { get; set; }
        
        [JsonProperty("previous")]
        public object Voltar { get; set; }
        
        [JsonProperty ("results")]
        public List<Pokemon> ListaPokemons { get; set; }

    }
}
