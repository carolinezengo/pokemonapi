﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.LinkLabel;

namespace PokemonApi.Model
{
   public class DadosPokemon
    {
        public int ID { get; set; }

        [JsonProperty("name")]
        public string NomePokemon { get; set; }

       [JsonProperty("sprites")]
      public Sprites Imagem { get; set; }

        [JsonProperty("Abilities")]
        public Ability[] Habilidade { get; set; }

        [JsonProperty("Types")]
        public TipoElemento[] Tipo { get; set; }

    }

}

