﻿using PokemonApi.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokemonApi
{
    public partial class PokemonVistaUnico : UserControl
    {
        public PokemonVistaUnico(string nome,  Image imagem,string tipo, string habilidade)
        {
            InitializeComponent();
         this.Nome.Text = nome;
            this.Imagem.Image = imagem;
            this.tipo.Text = tipo;
            this.Habilidade.Text = habilidade;
           
         
        }

        private void PokemonVista_Load(object sender, EventArgs e)
        {

        }

        private void Imagem_Click(object sender, EventArgs e)
        {

        }

    }
}
