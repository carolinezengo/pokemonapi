﻿using PokemonApi.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PokemonApi
{
    public partial class Form1 : Form
       
    {
        ApiRequest api = new ApiRequest();
       
        public Form1()
        {
            InitializeComponent();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CarregarDados();
            
        }

        public void CarregarDados()

        {
            flowLayoutPanel1.Controls.Clear();
            ListaPokemon pokemon = new ListaPokemon();
            pokemon = api.ObterLista();

               
                for (int i = 0; i < 10; i++)
                {
                     int id2 = i + 1;

                var pokemonVista = new PokemonVista(pokemon.ListaPokemons[i].NomePokemon,
                  pokemon.ListaPokemons[i].GetImage(), id2);

                pokemonVista.Click += (sender, e) => AbriForm2ComID(sender, e, id2);
                flowLayoutPanel1.Controls.Add(pokemonVista);



                 }

        }
      

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }

        private void AbriForm2ComID(object sender, EventArgs e, int id)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
