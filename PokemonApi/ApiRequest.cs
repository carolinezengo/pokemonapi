﻿using Newtonsoft.Json;
using PokemonApi.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PokemonApi
{
   public  class ApiRequest
    {
        public string url { get; set; }
        public int atual = 0;
       

        public ApiRequest()
        {

            url = "https://pokeapi.co/api/v2/pokemon?limit=10&offset=";
        }

        public ListaPokemon ObterLista()
        {

            var consulta = (HttpWebRequest)WebRequest.Create(url + atual);
            consulta.Method = "GET";
            consulta.ContentType = "application/json";
            consulta.Accept = "application/json";
            
            try 
            {
                using (WebResponse response = consulta.GetResponse())
                {
                    using(Stream stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }
                        using (StreamReader leitor = new StreamReader(stream))
                        {
                            string responseTexto = leitor.ReadToEnd();
                            ListaPokemon pokeLista = JsonConvert.DeserializeObject<ListaPokemon>(responseTexto);
                            atual = atual + 10;
                          
                            
                            return pokeLista;
                        }




                    }
                }
            }
            catch (Exception) {
                throw;
            }

            
        }

        public void  CarregarId()
        {
            ListaPokemon pokemon = new ListaPokemon();
            pokemon = ObterLista();

           

            foreach (var item in pokemon.ListaPokemons)
            {
                string pokemon2 = item.Link.OriginalString;
                Console.WriteLine(pokemon2);
            }
            
           
        }
    }
}
