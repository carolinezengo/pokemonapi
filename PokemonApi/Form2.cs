﻿using PokemonApi.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokemonApi
{
    public partial class Form2 : Form
    {
        public int pokemonId;

        
        
        public Form2(int id)
        {
            InitializeComponent();
            this.TopMost = true;
            this.pokemonId = id;
        }
        public Form2()
        {
            InitializeComponent();
            this.TopMost = true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            CarregarDados();
        }

        public void CarregarDados()

        {

            ApiRequestUnico api = new ApiRequestUnico(pokemonId);
            flowLayoutPanel1.Controls.Clear();
            DadosPokemon pokemon = new DadosPokemon();

            pokemon = api.Pokemon();


            foreach (var item in pokemon.Habilidade)
            {
                var habilidade = item.Habilidade1.Nome;
                foreach (var i in pokemon.Tipo)
                {
                    var tipo = i.Tipo.Nome;
                    flowLayoutPanel1.Controls.Add(new PokemonVistaUnico(pokemon.NomePokemon, pokemon.Imagem.GetImage(), habilidade, tipo));
                }

            }








        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

     
    }
}
