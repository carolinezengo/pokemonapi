﻿
namespace PokemonApi
{
    partial class PokemonVista
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.imagem = new System.Windows.Forms.PictureBox();
            this.nome = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imagem)).BeginInit();
            this.SuspendLayout();
            // 
            // imagem
            // 
            this.imagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imagem.Location = new System.Drawing.Point(24, 36);
            this.imagem.Name = "imagem";
            this.imagem.Size = new System.Drawing.Size(97, 90);
            this.imagem.TabIndex = 0;
            this.imagem.TabStop = false;
            this.imagem.Click += new System.EventHandler(this.imagem_Click);
            // 
            // nome
            // 
            this.nome.AutoSize = true;
            this.nome.Location = new System.Drawing.Point(53, 129);
            this.nome.Name = "nome";
            this.nome.Size = new System.Drawing.Size(35, 13);
            this.nome.TabIndex = 1;
            this.nome.Text = "label1";
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(53, 142);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(35, 13);
            this.ID.TabIndex = 2;
            this.ID.Text = "label1";
            // 
            // PokemonVista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ID);
            this.Controls.Add(this.nome);
            this.Controls.Add(this.imagem);
            this.Name = "PokemonVista";
            this.Size = new System.Drawing.Size(150, 164);
            this.Load += new System.EventHandler(this.PokemonVista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imagem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imagem;
        public System.Windows.Forms.Label nome;
        private System.Windows.Forms.Label ID;
    }
}
